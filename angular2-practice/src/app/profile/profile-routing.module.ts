import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProfileComponent }   from './profile.component';
import { AuthGuard } from '../_guards/index';

const ProfileRoutes: Routes = [
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [ RouterModule.forChild(ProfileRoutes) ],
  exports: [ RouterModule ]
})

export class ProfileRoutingModule {}
