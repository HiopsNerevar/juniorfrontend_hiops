import { BrowserModule, Title  } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ProfileComponent } from './profile.component';
import { ProfileRoutingModule } from './profile-routing.module';


@NgModule({
  declarations: [
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    ProfileRoutingModule
  ],
  exports: [
  ],
  providers: [ Title ],
  bootstrap: [ ]
})
export class ProfileModule {}
