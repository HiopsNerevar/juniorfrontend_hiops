import { Component, OnInit } from '@angular/core';

import { User } from '../_models/index';
import { UserService } from '../_services/index';

import { Title } from '@angular/platform-browser';


@Component({
  moduleId: module.id,
  selector: 'user-profile-page',
  templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {
    users: User[] = [];
    pageTitle = 'Profile';

    constructor(private userService: UserService, private title: Title) {}

    ngOnInit() {
        // get users from secure api end point
        this.userService.getUsers()
            .subscribe(users => {
                this.users = users;
            });

        let currentTitle = this.title.getTitle();
        this.title.setTitle(this.pageTitle);
    }
}
