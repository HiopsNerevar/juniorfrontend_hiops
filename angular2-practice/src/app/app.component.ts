import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: './app.component.html'
})

export class AppComponent {
  siteName = 'MuzikPeople';
  pageTitle = 'Main Page';
    constructor(
        private title: Title
    ){
        let currentTitle = this.title.getTitle();
        this.title.setTitle(this.pageTitle);
    }
}
