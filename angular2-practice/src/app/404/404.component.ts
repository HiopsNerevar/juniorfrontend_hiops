import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AppComponent } from '../app.component';

@Component({
  selector: 'pagenotfound',
  templateUrl: './404.component.html'
})
export class PageNotFoundComponent {
  pageTitle = 'Page not Found';

  constructor(
      private title: Title
  ){
      let currentTitle = this.title.getTitle();
      this.title.setTitle(this.pageTitle);
  }

}
