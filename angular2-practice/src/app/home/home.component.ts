import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AppComponent } from '../app.component';


@Component({
  selector: 'home-page',
  templateUrl: './home.component.html'
})
export class HomeComponent {
  pageTitle = 'Homepage';
  pageclass = 'home-page';

  constructor(
      private title: Title
  ){
      let currentTitle = this.title.getTitle();
      this.title.setTitle(this.pageTitle);
  }

}
