//System
import { BrowserModule, Title  } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }    from '@angular/forms';
import { HttpModule }    from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';

//Services
import { ApiService } from './shared/services/api.service';
import { SomeService } from './shared/services/some.service';

//Modules
import { SharedModule } from './shared/shared.module';
import { FeaturesModule } from './features/features.module';
import { HomeModule } from './home/home.module';
import { ProfileModule } from './profile/profile.module';

//Components
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { FooterComponent } from './footer/footer.component';
import { ModalsComponent, LoginComponent } from './modals/index';
import { PageNotFoundComponent } from './404/404.component';

// used to create fake backend
import { fakeBackendProvider } from './_helpers/fake-backend';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { BaseRequestOptions } from '@angular/http';

import { AuthGuard } from './_guards/index';
import { AuthenticationService, UserService } from './_services/index';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    NavComponent,
    ModalsComponent,
    LoginComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    SharedModule,
    FeaturesModule,
    HomeModule,
    ProfileModule,
    AppRoutingModule //everytime last!
  ],
  providers: [
    Title,
    ApiService,
    SomeService,
    AuthGuard,
    AuthenticationService,
    UserService,

    // providers used to create fake backend
    // fakeBackendProvider,
    MockBackend,
    BaseRequestOptions
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
