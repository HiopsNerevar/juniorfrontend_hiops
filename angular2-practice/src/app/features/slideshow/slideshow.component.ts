import { Component, OnInit, AfterViewInit} from '@angular/core';
import {Http} from '@angular/http';

import { SlideshowStore } from './slideshow.store';


@Component({
  selector: 'app-slideshow',
  templateUrl: './slideshow.component.html'
})

export class SlideshowComponent implements OnInit, AfterViewInit {
  products: any = [];
  page: number = 0;
  itemsCount: number = 9;

  constructor(private storage: SlideshowStore) {
}

  ngOnInit () {
    this.carousel();
  }

  ngAfterViewInit () {

  }

  carousel() {
    this.storage.getSlideshowImage(this.page, this.itemsCount).then(products => {
      this.products = products;
    });
  }

}
