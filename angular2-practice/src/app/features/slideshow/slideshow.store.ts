import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { SomeService } from '../../shared/services/some.service';

@Injectable()
export class SlideshowStore {
    subscriptions: Subscription[] = [];
    private products: any = [];

    constructor(private someService: SomeService) {
    }

    getSlideshowImage(page: number, itemsCount: number): Promise<any> {
        return (!this.products.lenght) ? this.someService.getSlideshowImage().then(products => {
          this.products = products;
          return products.slice(page, itemsCount);
        }) :
        new Promise(resolve => this.products.slice(page, itemsCount));
    }
}
