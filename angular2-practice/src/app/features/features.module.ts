import { BrowserModule, Title  } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FeaturesComponent } from './features.component';
import { GalleryComponent } from './gallery/gallery.component';
import { GalleryStore } from './gallery/gallery.store';
import { SlideshowComponent } from './slideshow/slideshow.component';
import { SlideshowStore } from './slideshow/slideshow.store';

import { FeatureRoutingModule } from './features-routing.module';

@NgModule({
  declarations: [
    FeaturesComponent,
    GalleryComponent,
    SlideshowComponent
  ],
  imports: [
    BrowserModule,
    FeatureRoutingModule
  ],
  exports: [
  ],
  providers: [ Title, GalleryStore, SlideshowStore ],
  bootstrap: [ ]
})
export class FeaturesModule {}
