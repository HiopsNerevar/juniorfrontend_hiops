import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'features-page',
  templateUrl: './features.component.html'
})
export class FeaturesComponent {
  pageTitle = 'Features';
  pageclass = 'features-page';

  constructor(
      private title: Title
  ){
      let currentTitle = this.title.getTitle();
      this.title.setTitle(this.pageTitle);
  }

}
