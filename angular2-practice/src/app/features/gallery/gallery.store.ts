import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { SomeService } from '../../shared/services/some.service';

@Injectable()
export class GalleryStore {
    subscriptions: Subscription[] = [];
    private products: any = [];

    constructor(private someService: SomeService) {
    }

    getGalleryImage(page: number, itemsCount: number): Promise<any> {
        return (!this.products.lenght) ? this.someService.getGalleryImage().then(products => {
          this.products = products;
          return products.slice(0, itemsCount);
        }) :
        new Promise(resolve => this.products.slice(page, itemsCount));
    }
}
