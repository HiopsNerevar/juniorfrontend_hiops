import { Component, OnInit, AfterViewInit } from '@angular/core';
import {Http} from '@angular/http';

import { GalleryStore } from './gallery.store';

@Component({
  selector: 'gallery-component',
  templateUrl: './gallery.component.html'
})

export class GalleryComponent implements OnInit, AfterViewInit {
  products: any = [];
  page: number = 0;
  itemsCount: number = 9;

  constructor(private storage: GalleryStore) {
}

  ngOnInit () {
    this.loadMore();
  }

  ngAfterViewInit () {

  }

  loadMore() {
    this.storage.getGalleryImage(this.page, this.itemsCount).then(products => {
      this.products = products;
      this.page++;
    });
  }
}
