import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FeaturesComponent }   from './features.component';

const FeatureRoutes: Routes = [
  { path: 'features', component: FeaturesComponent }
];

@NgModule({
  imports: [ RouterModule.forChild(FeatureRoutes) ],
  exports: [ RouterModule ]
})

export class FeatureRoutingModule {}
