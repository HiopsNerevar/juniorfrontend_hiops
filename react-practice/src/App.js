import React, { Component } from 'react';
import $ from 'jquery';
import Header from './header/header';
import Main from './main/main';
import Footer from './footer/footer';
import logo from './logo.svg';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="all-wrapper">
          <Header />
          <Main />
          <Footer />
      </div>
    );
  }
}

export default App;
