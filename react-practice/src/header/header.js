import React, { Component } from 'react';
import $ from 'jquery';
class Header extends Component{
  render() {
    return (
<header className="container">
  <nav className="nav navbar-expand-lg">
    <a className="navbar-brand" href="#">
      <img src="./img/logo.png" width="85" alt="" />
    </a>
    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse nav-container" id="navbarNav">
      <ul className="navbar-nav">
        <li className="nav-item">
          <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
        </li>
        <li className="nav-item  active">
          <a className="nav-link" href="#">Portfolio</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#">Blog</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#">About Us</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#">Contact</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" data-toggle="modal" data-target="#loginModal" href="#">LogIn</a>
        </li>
      </ul>
    </div>
  </nav>
  <div className="header-image">
    <h1 className="header-image__h1">Portfolio</h1>
  </div>
</header>
);
}
}

export default Header;
