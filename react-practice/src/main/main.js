import React, { Component } from 'react';
import $ from 'jquery';

class Main extends Component{
  constructor(props) {
    super(props);
    this.state = {products: []};
    this.products = [];

    }
  componentDidMount() {
    var that = this;
    $.ajax({
      url: 'https://jsonplaceholder.typicode.com/photos',
      method: 'GET',
      success: function(data, status, xhr) {
        //that.setState(that.products: data);
        var slice = data.slice(0, 9);
        that.products = slice.map((item) => {
          <div class="col-md-4">
            <div class="image-cards__image" id={item.id}>
              <img src={item.url} alt={item.title} class="img-fluid" />
              <div class="image-cards__image__hover" ></div>
              <div class="image-cards__image__button"><i class="image-cards__image__icon fa fa-arrow-right"></i></div>
            </div>
          </div>
        });
      },
      error: function() {}
    });
  }

  render() {
    return (
      <div className="main">
      <section className="container headline text-center">
        <h2>We create qualyty designs</h2>
        <h3>We specialize in Web Design / Development and Graphic Design</h3>
      </section>

      <section className="image-carousel container">
    		<div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
    		  <ol className="carousel-indicators">
    		  </ol>
    		  <div className="carousel-inner">
    			<div className="overlay-carousel"></div>
    		  </div>
    		  <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    			<span className="carousel-control-prev-icon" aria-hidden="true"></span>
    			<span className="sr-only">Previous</span>
    		  </a>
    		  <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    			<span className="carousel-control-next-icon" aria-hidden="true"></span>
    			<span className="sr-only">Next</span>
    		  </a>
    		</div>
      </section>

      <section className="container">
        <div className="filter-menu">
          <ul className="filter-items">
            <li className="filter-items__item active" id="filter-all">All</li>
            <li className="filter-items__item" id="filter-website">Website</li>
            <li className="filter-items__item" id="filter-logo">Logo</li>
            <li className="filter-items__item" id="filter-uikit">UI kit</li>
            <li className="filter-items__item" id="filter-photo">Photo</li>
            <li className="filter-items__item" id="filter-app">App Design</li>
          </ul>
        </div>
        <div className="image-cards">
          <div className="row align-items-center">
            <div className="col-md-4">
              <div className="image-cards__image" id="logo">
                <img src="./img/products/1.png" alt="" className="img-fluid" />
                <div className="image-cards__image__hover"></div>
                <div className="image-cards__image__button"><i className="image-cards__image__icon fa fa-arrow-right"></i></div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="image-cards__image" id="website">
                <img src="./img/products/2.png" alt="Lorem ipsum dolor sit ammet" className=" img-fluid" />
                <div className="image-cards__image__hover"></div>
                <div className="image-cards__image__button"><i className="image-cards__image__icon fa fa-arrow-right"></i></div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="image-cards__image" id="uikit">
                <img src="./img/products/3.png" alt="Lorem ipsum dolor sit ammet" className=" img-fluid" />
                <div className="image-cards__image__hover"></div>
                <div className="image-cards__image__button"><i className="image-cards__image__icon fa fa-arrow-right"></i></div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="image-cards__image" id="uikit">
                <img src="./img/products/4.png" alt="Lorem ipsum dolor sit ammet" className=" img-fluid" />
                <div className="image-cards__image__hover"></div>
                <div className="image-cards__image__button"><i className="image-cards__image__icon fa fa-arrow-right"></i></div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="image-cards__image" id="website">
                <img src="./img/products/5.png" alt="Lorem ipsum dolor sit ammet" className=" img-fluid" />
                <div className="image-cards__image__hover"></div>
                <div className="image-cards__image__button"><i className="image-cards__image__icon fa fa-arrow-right"></i></div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="image-cards__image" id="photo">
                <img src="./img/products/6.png" alt="Lorem ipsum dolor sit ammet" className=" img-fluid" />
                <div className="image-cards__image__hover"></div>
                <div className="image-cards__image__button"><i className="image-cards__image__icon fa fa-arrow-right"></i></div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="image-cards__image" id="photo">
                <img src="./img/products/7.png" alt="Lorem ipsum dolor sit ammet" className=" img-fluid" />
                <div className="image-cards__image__hover"></div>
                <div className="image-cards__image__button"><i className="image-cards__image__icon fa fa-arrow-right"></i></div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="image-cards__image" id="appdesign">
                <img src="./img/products/8.png" alt="Lorem ipsum dolor sit ammet" className=" img-fluid" />
                <div className="image-cards__image__hover"></div>
                <div className="image-cards__image__button"><i className="image-cards__image__icon fa fa-arrow-right"></i></div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="image-cards__image" id="uikit">
                <img src="./img/products/9.png" alt="Lorem ipsum dolor sit ammet" className=" img-fluid" />
                <div className="image-cards__image__hover"></div>
                <div className="image-cards__image__button"><i className="image-cards__image__icon fa fa-arrow-right"></i></div>
              </div>
            </div>
            {this.products}
          </div>
        </div>
      <div className="text-center"><button className="btn btn-success upload-images">Загурзить еще <i className="fa fa-plus-circle"></i></button></div>

      </section>
      </div>
);
}
}

export default Main;
