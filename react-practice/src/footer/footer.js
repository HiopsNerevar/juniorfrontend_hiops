import React, { Component } from 'react';
import $ from 'jquery';

class Footer extends Component{
  render() {
    return (

        <footer>
          <section className="container footer-menu">
            <div className="row">
              <div className="col-md-4">
                <div className="footer-menu__about">
                  <h4>About Us</h4>
                  <p className="about-us">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
                  <a href="#" className="readmore">Learn more</a>
                </div>
                <div className="footer-menu__photo">
                  <h4>Photo stream</h4>
                  <div className="footer-menu__photo__block d-flex justify-content-between">
                    <div className="stream-cards__image"><img src="./img/products/1.png" alt="Lorem ipsum dolor sitclassName= ammet" className=" img-fluid" /></div>
                    <div className="stream-cards__image"><img src="./img/products/2.png" alt="Lorem ipsum dolor sitclassName= ammet" className=" img-fluid" /></div>
                    <div className="stream-cards__image"><img src="./img/products/3.png" alt="Lorem ipsum dolor sitclassName= ammet" className=" img-fluid" /></div>
                  </div>
                  <div className="footer-menu__photo__block d-flex justify-content-between">
                    <div className="stream-cards__image"><img src="./img/products/4.png" alt="Lorem ipsum dolor sitclassName= ammet" className=" img-fluid" /></div>
                    <div className="stream-cards__image"><img src="./img/products/5.png" alt="Lorem ipsum dolor sitclassName= ammet" className=" img-fluid" /></div>
                    <div className="stream-cards__image"><img src="./img/products/6.png" alt="Lorem ipsum dolor sitclassName= ammet" className=" img-fluid" /></div>
                  </div>
                </div>
              </div>
              <div className="col-md-5">
                <div className="footer-menu__latest">
                  <h4>Latest Week</h4>
                  <div className="media">
                    <i className="fa fa-twitter"></i>
                    <div className="media-body">
                      <p>Cras sit amet nibh <a href="#">@libero</a>, in gravida nulla.>, in gravida nullaa.>, in gravida nulla</p>
                      <small>5 hours ago</small>
                    </div>
                  </div>
                  <div className="media">
                    <i className="fa fa-twitter"></i>
                    <div className="media-body">
                      <p>Cras sit amet nibh <a href="#">@libero</a>, in gravida nulla>, in gravida nulla.a.>, in gravida nulla</p>
                      <small>5 hours ago</small>
                    </div>
                  </div>
                  <div className="media">
                    <i className="fa fa-twitter"></i>
                    <div className="media-body">
                      <p>Cras sit amet nibh <a href="#">@libero</a>, in gravida nulla>, in gravida nulla.a.>, in gravida nulla</p>
                      <small>5 hours ago</small>
                    </div>
                  </div>
                  <div className="media">
                    <i className="fa fa-twitter"></i>
                    <div className="media-body">
                      <p>Cras sit amet nibh <a href="#">@libero</a>, in gravida nulla>, in gravida nulla.a.>, in gravida nulla</p>
                      <small>5 hours ago</small>
                    </div>
                  </div>
                  <div className="media">
                    <i className="fa fa-twitter"></i>
                    <div className="media-body">
                      <p>Cras sit amet nibh <a href="#">@libero</a>, in gravida nulla.>, in gravida nullaa.>, in gravida nulla</p>
                      <small>5 hours ago</small>
                    </div>
                  </div>

                </div>
                <div className="footer-menu__social d-flex">
                  <h4>Social Connecting</h4>
                  <a href="#"><i className="fa fa-twitter bg-inverse text-white"></i></a>
                  <a href="#"><i className="fa fa-twitter bg-inverse text-white"></i></a>
                  <a href="#"><i className="fa fa-twitter bg-inverse text-white"></i></a>
                  <a href="#"><i className="fa fa-twitter bg-inverse text-white"></i></a>
                  <a href="#"><i className="fa fa-twitter bg-inverse text-white"></i></a>
                  <a href="#"><i className="fa fa-twitter bg-inverse text-white"></i></a>
                </div>
              </div>
              <div className="col-md-3">
                <div className="footer-menu__contact">
                  <h4>Contact Info</h4>
                  <div className="footer-menu__contact__block">
                    <div className="media">
                      <i className="footer-menu__contact__icon fa fa-twitter"></i>
                      <div className="media-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                      </div>
                    </div>
                    <div className="media">
                      <i className="footer-menu__contact__icon fa fa-twitter"></i>
                      <div className="media-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                      </div>
                    </div>
                    <div className="media">
                      <i className="footer-menu__contact__icon fa fa-twitter"></i>
                      <div className="media-body">
                        <p>Cras sit amet nibh <a href="#">@libero</a></p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="footer-menu__follow">
                  <h4>Follow Us</h4>
                  <p className="follow">Lorem ipsum dolor sit amet, consectetur adipisicing elit., consectetur adipisicing eli</p>
                  <form action="" className="footer-menu__follow__form d-flex">
                    <input type="text" className="footer-menu__follow__form__input" />
                    <button type="button" name="button" className="footer-menu__follow__form__button"><i className="footer-menu__follow__form__button__icon fa fa-envelope"></i></button>
                  </form>


                </div>
              </div>
            </div>
          </section>
          <section className="copyright d-flex">
            <div className="container d-flex justify-content-between  align-items-center">
              <p>Copyright 2013 All Right Reserved</p>
              <ul className="navbar-nav flex-row">
                <li className="nav-item">
                  <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
                </li>
                <li className="nav-item  active">
                  <a className="nav-link" href="#">Portfolio</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">Blog</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">About Us</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">Contact</a>
                </li>
              </ul>
            </div>
          </section>
        </footer>

);
}
}

export default Footer;
