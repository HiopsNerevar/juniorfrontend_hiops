var page = 0;
var itemsCount = 9;

function imgGet() {
  $.ajax({
    url: 'https://jsonplaceholder.typicode.com/photos',
    method: 'GET',
    success: function(data, status, xhr) {
      addImgaeOnPage(data.slice(page * itemsCount, (page + 1) * itemsCount));
      page++;
      myModal();
    },
    error: function() {}
  });
}

function addImgaeOnPage(data) {
  var galleryElem = $(".image-cards");
  for (var i = 0; i < data.length; i++) {
    galleryElem.children().append('<div class="col-md-4">' +
      '<div class="image-cards__image hided" id="' + data[i].id + '">' +
      '<img src="' + data[i].url + '" alt="' + data[i].title + '" class="img-fluid">' +
      '<div class="image-cards__image__hover" ></div>' +
      '<div class="image-cards__image__button"><i class="image-cards__image__icon fa fa-arrow-right"></i></div>' +
      '</div>' +
      '</div>');
  }

  setTimeout(() => {
    var opacity = $(".image-cards__image");
    var i = (page - 1) * itemsCount;
    (function loop() {
      opacity[i].className = 'image-cards__image';
      if (++i < opacity.length) {
        setTimeout(loop, 100);
      }
    })();
  }, 0);


  $(".image-cards__image").off("click", openModal);
}

$(".upload-images").on("click", imgGet);
