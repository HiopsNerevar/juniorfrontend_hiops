$(document).ready(loadGet);


function loadGet() {
  var page = 0;
  var itemsCount = 9;
  $.ajax({
      url: 'http://www.splashbase.co/api/v1/images/latest',
      method: 'GET',
      success: function(data, status, xhr) {
        fillCarousel(data.images.slice(page * itemsCount,(page + 1) * itemsCount));
      }, error: function() {
      }
    });
}


function fillCarousel(data) {
var carouselIndicators = $(".carousel-indicators");
var carouselElem = $(".carousel-inner");
for (var i = 0; i < data.length; i++) {
 carouselIndicators.append(`<li data-target="#carouselExampleIndicators" data-slide-to="'+ [i] +'" class="${((i === 0) ? ' active' : '')}"></li>`);
}
for (var i = 0; i < data.length; i++) {
carouselElem.append(`<div class="carousel-item  ${((i === 0) ? ' active' : '')} ">`
      + '<img src="' + data[i].large_url + '" alt="' + data[i].title + '" class="img-fluid">'
     + ' <div class="carousel-caption d-none d-md-block">'
        + '<h3>' + data[i].title + '</h3>'
        + '<p>' + data[i].title + '</p>'
      + '</div>'
    + '</div>');
 }
}
