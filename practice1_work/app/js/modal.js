myModal();
function myModal() {
  $(".image-cards__image").on("click", openModal);
  $(".overlay").on("click", closeModal);
  $(".close-btn").on("click", closeModal);
}

function openModal() {
    var imageSrc = $(this).children("img").attr("src");
	var imageTitle = $(this).children("img").attr("alt");
    $(".image-frame").append('<img src=' + imageSrc + ' class="img-fluid">'
	+ '<div class="descr"><p>Описание: ' + imageTitle + '</p></div>');
    $("#modal-window").addClass(" active ");
    $(".overlay").css("display", "block");
  }

  function closeModal() {
    $("#modal-window").removeClass(" active ");
    $(".overlay").css("display", "none");
    $(".image-frame").empty();
  }