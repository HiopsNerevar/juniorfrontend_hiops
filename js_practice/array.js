function range(start, end, step) {
	if (step === undefined) step = 1;
	var array = [];
	if (step > 0) {
		for (var i = start; i <= end; i += step) {
		array.push(i)
		}
	} else {
		for (var i = start; i >= end; i += step) {
		array.push(i)
		}	
	}
	return array;
}

function sum(array) {
	var result = 0;
	for (var i = 0; i < array.length; i++) {
		result += array[i];
	}
	return result;
}

console.log(range(1, 10))
console.log(range(5, 2, -1));
console.log(sum(range(1, 10)));

console.log("#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-");


function reverseArray(array) {
	var reverse = array.reverse();
	return reverse;
}

function reverseArraySecond(array) {
	var reverse = [];
	for (var i = array.length - 1; i >= 0; i--) {
		reverse.push(array[i]);
	}
	return reverse;
}
function reverseArrayThird(array) {
	var reverse = [];
	var i, j;
	for (i = 0, j = array.length - 1; i < j; i++, j--) {
		reverse = array[i];
		array[i] = array[j];
		array[j] = reverse;
	}
	return array;
}

function reverseArrayInPlace(array) {
  for (var i = 0; i < (array.length / 2); i++) {
    var reverse = array[i];
    array[i] = array[array.length - 1 - i];
    array[array.length - 1 - i] = reverse;
  }
  return array;
}

console.log(reverseArray(["A", "B", "C"]));
console.log(reverseArraySecond(["A", "B", "C"]));
console.log(reverseArrayThird([1, 2, 3, 4, 5]));
var arrayValue = [1, 2, 3, 4, 5];
reverseArrayInPlace(arrayValue);
console.log(arrayValue);

console.log("#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-");

function arrayToList(array) {
  var list = null;
  for (var i = array.length - 1; i >= 0; i--)
    list = {value: array[i], rest: list};
  return list;
}

function listToArray(list) {
  var array = [];
  for (var node = list; node; node = node.rest)
    array.push(node.value);
  return array;
}
function prepend(value, list) {
  return {value: value, rest: list};
}

function nth(list, n) {
  if (!list) {
	  return undefined;
  } else if (n == 0) {
	  return list.value;
  } else {
	  return nth(list.rest, n - 1);
	}    
}


console.log(arrayToList([10, 20]));
console.log(listToArray(arrayToList([10, 20, 30])));
console.log(prepend(10, prepend(20, null)));
console.log(nth(arrayToList([10, 20, 30]), 1));

console.log("#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-");

function deepEqual(first, second) {
	if (first === second) return true;
	if (first == null || typeof first != "object" || second == null || typeof second != "object") return false;
	
	var firstProps = 0, secondProps = 0;

	for (var prop in first)
		firstProps += 1;

  for (var prop in second) {
    secondProps += 1;
    if (!(prop in first) || !deepEqual(first[prop], second[prop]))
      return false;
  }

  return firstProps == secondProps;
}

var obj = {here: {is: "an"}, object: 2};
console.log(deepEqual(obj, obj));
// → true
console.log(deepEqual(obj, {here: 1, object: 2}));
// → false
console.log(deepEqual(obj, {here: {is: "an"}, object: 2}));
// → true