function min(a, b) {
  if (a <= b) {
    return a + " меньше " + b;
  } else {
    return a + " больше " + b;
  }
}
console.log(min(0, 10));
console.log(min(0, -10));

console.log("#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-");

function isEven(num) {
  if (num === 1) {
    return true;
  } else if (num === 0) {
    return false;
  } else if (num < 0) {
    return isEven(-num)
  } else {
    return isEven(num - 2);
  }
}
console.log(isEven(-3));
console.log(isEven(50));
console.log(isEven(75));

console.log("#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-");

function countChar(someString, someSym) {
  symbolPos = 0;
  for (var i = 0; i < someString.length; i++)
        if (someString.charAt(i) === someSym) {
            symbolPos += 1;
        }
  return symbolPos;
}
console.log(countChar("wqrtqiwrjoiqjqowijqonfiqwoiqiwjfoiwroi","i"));


