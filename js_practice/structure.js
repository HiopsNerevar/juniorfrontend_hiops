var i = '#';
for (i; i.length <= 7; i += '#') {
  console.log(i);
};
console.log("#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-");

var a = 1;
for (a; a <= 50; a++) {
  var fizz = '';
  if (a % 3 === 0) {
    fizz += 'Fizz';
    if (a % 5 === 0 && a % 3 !== 0) {
      fizz += 'Buzz';
    }
  }
  if (a % 5 === 0) {
    fizz += 'Buzz';
  }
  console.log(fizz || a);
};
console.log("#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-");

var chess = '';
var board = 8;
for (var line = 0; line < board; line++) {
  for (var cols = 0; cols < board; cols++) {
    if ((line + cols) % 2 == 0) {
      chess += '#';
    } else {
      chess += ' ';
    }
  }
  chess += '\n';
};
console.log(chess);
console.log("#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-");